import "./App.css";
import React from "react";
import Header from "./components/Header/Header"

class App extends React.Component {

  state = {
    name: "rafiekhana"
  };

  render() {
    return (
      <div className="App">
        <div className="page">
          
          <Header name={this.state.name} clickName={() => this.setState({name: "Rafie"})} />
          
          <div className="hero">
            <div className="container">
              <div className="hero__wrapper">
                <div className="hero__content">
                  <div className="hero__text">
                    <h1 className="hero__title">
                      Let's go <span>Shopping</span>
                    </h1>
                    <p className="hero__description">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Dolorum, esse! Eligendi veritatis animi magni voluptas
                      aliquam commodi quisquam illum nemo in quaerat itaque,
                      tempore porro ipsa, recusandae deserunt illo suscipit!
                    </p>
                  </div>
                  <div className="hero__actions">
                    <button className="btn" onClick={() => this.setState({name: "Rafie"})}>Shop now!</button>
                    <input type="text" onChange={(e) => this.setState({name: e.target.value})} />
                    
                  </div>
                </div>
                <div className="hero__image">
                  <img src="./assets/hero.jpg" alt="hero" />
                </div>
              </div>
            </div>
          </div>

          <div className="location" id="location">
            <div className="container">
              <div className="location__content">
                <div className="location__title">
                  <strong>Location</strong>
                </div>
                <div className="location__cards">
                  <div className="card">
                    <div className="card__wrapper">
                      <div className="card__header">
                        <img src="./assets/tokped.png" alt="tokped" />
                        <div className="card__point">
                          <span>4.6</span>
                        </div>
                      </div>
                      <div className="card__body">
                        <div className="card__title">
                          Tokopedia
                        </div>
                        <div className="card__description">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit.
                          Ullam ut illum dolores dignissimos ipsa ducimus harum
                          aspernatur quam in eveniet, consequatur inventore
                          officiis, tempore pariatur incidunt, vero adipisci quae
                          tenetur.
                        </div>
                      </div>
                    </div>
                  </div>
  
                  <div className="card">
                    <div className="card__wrapper">
                      <div className="card__header">
                        <img src="./assets/bukalapak.png" alt="bukalapak" />
                        <div className="card__point">
                          <span>4.7</span>
                        </div>
                      </div>
                      <div className="card__body">
                        <div className="card__title">
                          Bukalapak
                        </div>
                        <div className="card__description">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit.
                          Ullam ut illum dolores dignissimos ipsa ducimus harum
                          aspernatur quam in eveniet, consequatur inventore
                          officiis, tempore pariatur incidunt, vero adipisci quae
                          tenetur.
                        </div>
                      </div>
                    </div>
                  </div>
  
                  <div className="card">
                    <div className="card__wrapper">
                      <div className="card__header">
                        <img src="./assets/shopee.png" alt="shopee" />
                        <div className="card__point">
                          <span>4.6</span>
                        </div>
                      </div>
                      <div className="card__body">
                        <div className="card__title">
                          Shopee
                        </div>
                        <div className="card__description">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit.
                          Ullam ut illum dolores dignissimos ipsa ducimus harum
                          aspernatur quam in eveniet, consequatur inventore
                          officiis, tempore pariatur incidunt, vero adipisci quae
                          tenetur.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          <div className="footer">©2021 office-hour-assestment-1</div>
        </div>
      </div>
    );
  }
  
}

export default App;
