import React from 'react';
import "./Header.css";

class Header extends React.Component {
  render() {
    return (
      <div>
        <div className="header">
            <div className="git__link">
              {this.props.name}
            </div>
            <button onClick={this.props.clickName}>Click Me</button>
          </div>
      </div>
    )
  }
}

export default Header;